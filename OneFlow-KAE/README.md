# 模型炼知（Model Knowledge Amalgamation, KA）
## Introduction
  模型炼知是用于知识融合和模型迁移性度量,而建立的轻量级算法包,详情请见 [KAE](https://github.com/zju-vipa/KamalEngine/tree/master/examples)。

## Table of contents
   * [Introduction](#Introduction)
   * [Algorithms](#Algorithms)
   * [Authors](#Authors)

## Algorithms
####  0. 数据集
   * data/StanfordCars: https://ai.stanford.edu/~jkrause/cars/car_dataset.html
   * data/StanfordDogs: http://vision.stanford.edu/aditya86/ImageNetDogs/
   * data/FGVCAircraft: http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/
   * data/Flowers102:   http://www.robots.ox.ac.uk/~vgg/data/fgvc-aircraft/
#### 1. 依赖包
   * [requirements.txt](requirements.txt)
#### 2. 算法
   * TW_Layerwise
   ```bash
      python3 TTL.py --car_ckpt ./ckpt/car_res50_model stanford_dogs  --dog_ckpt ./ckpt/dog_res50_model
   ```
   * TH_Layerwise
   ```bash
      python3 THL.py --car_ckpt ./ckpt/car_res50_model stanford_dogs  --dog_ckpt ./ckpt/dog_res50_model --aircraft_ckpt ./ckpt/aircraft_res50_model
   ```
   * TF_Layerwise
   ```bash
     python3 TFL.py --car_ckpt ./ckpt/car_res50_model stanford_dogs  --dog_ckpt ./ckpt/dog_res50_model --aircraft_ckpt ./ckpt/aircraft_res50_model --flower_ckpt ./ckpt/flower_res50_model
   ```
   * flow
   ```bash
      pytorch weights convert oneflow
   ```
#### 3. 权重
     文件夹MODEL_ckpt保存的是三个模型重组/炼知算法的权重,oneflow格式,供复现时参考。ckpt文件夹为pytorch权重转为oneflow权重,用于教师模型加载权重，内存占用较大，改为放在下方链接。
   * 教师模型权重 [链接]：https://pan.baidu.com/s/19pRLZKQvjgEQ7CuD_RiPPw 提取码：FLOW

## Authors
This project is simplified by KAE, which is developed by [VIPA Lab](http://vipazoo.cn) from Zhejiang University and [Zhejiang Lab](http://www.zhejianglab.com/)
