from .functional import *

class KLDiv(object):
    def __init__(self, T=1.0):
        self.T = T
    
    def __call__(self, logits, targets):
        return kldiv( logits, targets, T=self.T )