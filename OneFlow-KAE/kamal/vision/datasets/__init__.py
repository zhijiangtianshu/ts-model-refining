from .stanford_cars import StanfordCars
from .stanford_dogs import StanfordDogs
from .flowers102 import Flowers102
from .sun397 import SUN397
from .fgvc_aircraft import FGVCAircraft