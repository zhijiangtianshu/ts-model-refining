import os
import glob
from PIL import Image
import numpy as np
from scipy.io import loadmat

from oneflow.utils import data
import random


class StanfordCars(data.Dataset):
    """Dataset for Stanford Cars
    """

    urls = {'cars_train.tgz':     'http://imagenet.stanford.edu/internal/car196/cars_train.tgz',
            'cars_test.tgz':       'http://imagenet.stanford.edu/internal/car196/cars_test.tgz',
            'car_devkit.tgz':      'https://ai.stanford.edu/~jkrause/cars/car_devkit.tgz',
            'cars_test_annos_withlabels.mat': 'http://imagenet.stanford.edu/internal/car196/cars_test_annos_withlabels.mat'}

    def __init__(self, root, split='train', s=0.5, download=False, transform=None, target_transform=None):
        self.root = os.path.abspath( os.path.expanduser(root) )
        self.split = split
        self.transform = transform
        self.target_transform = target_transform

        if download:
            self.download()

        if self.split == 'train':
            annos = os.path.join(self.root, 'devkit', 'cars_train_annos.mat')
        else:
            annos = os.path.join(self.root, 'devkit',
                                 'cars_test_annos_withlabels.mat')

        annos = loadmat(annos)
        size = len(annos['annotations'][0])

        self.files = glob.glob(os.path.join(
            self.root, 'cars_'+self.split, '*.jpg'))
        self.files.sort()

        """if split == 'train':
            self.files = self.sample_by_class(s=s)"""

        self.labels = np.array([int(l[4])-1 for l in annos['annotations'][0]])

        lbl_annos = loadmat(os.path.join(self.root, 'devkit', 'cars_meta.mat'))

        self.object_categories = [str(c[0])
                                  for c in lbl_annos['class_names'][0]]

        print('Stanford Cars, Split: %s, Size: %d' %
              (self.split, self.__len__()))

    def __len__(self):
        return len(self.files)

    def __getitem__(self, idx):
        img = Image.open(os.path.join(self.root, 'Images',
                                      self.files[idx])).convert("RGB")
        lbl = self.labels[idx]
        if self.transform is not None:
            img = self.transform(img)
        if self.target_transform is not None:
            lbl = self.target_transform(lbl)
        return img, lbl
    
    def sample_by_class(self, s):
        class_dit = {}
        for file in self.files:
            class_name = file.split('/')[0]
            if class_name not in class_dit.keys():
                class_dit[class_name] = []
            class_dit[class_name].append(file)
        
        files = []
        for key in class_dit.keys():
            n = len(class_dit[key])
            random.shuffle(class_dit[key])
            files += class_dit[key][:int(n*s)]
        return files
